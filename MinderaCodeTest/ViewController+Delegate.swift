//
//  ViewController+Delegate.swift
//  MinderaCodeTest
//
//  Created by Stuart Thomas on 18/01/2021.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let halfScreenSize = UIScreen.main.bounds.width/2
    return CGSize(width: halfScreenSize,
                  height: halfScreenSize)
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      didSelectItemAt indexPath: IndexPath) {
    if isUsingCoreDataImages {
      if let unwrappedImageData = coreDataImages?[indexPath.row].large {
        setupFullScreenImage(data: unwrappedImageData)
      }
    } else {
      if let imageUrlString = images?[indexPath.row].large,
         let imageUrl = URL(string: imageUrlString) {
        let data = try? Data(contentsOf: imageUrl)
        setupFullScreenImage(data: data)
      }
    }
  }
  
  @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
    self.navigationController?.isNavigationBarHidden = false
    self.tabBarController?.tabBar.isHidden = false
    sender.view?.removeFromSuperview()
  }
  
  private func setupFullScreenImage(data: Data?) {
    let newImageView = UIImageView(image: UIImage(data: data!))
    setupFullScreenImageView(newImageView)
    self.view.addSubview(newImageView)
    self.navigationController?.isNavigationBarHidden = true
    self.tabBarController?.tabBar.isHidden = true
  }
  
  private func setupFullScreenImageView(_ imageView: UIImageView) {
    imageView.frame = UIScreen.main.bounds
    imageView.backgroundColor = .black
    imageView.contentMode = .scaleAspectFit
    imageView.isUserInteractionEnabled = true
    let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
    imageView.addGestureRecognizer(tap)
  }
  
}
