//
//  DataFetcher.swift
//  MinderaCodeTest
//
//  Created by Stuart Thomas on 17/01/2021.
//

import Foundation

public struct ResponseObject: Codable {
  public let photos: Photos
  
  public init(photos: Photos) {
    self.photos = photos
  }
}

public struct Photos: Codable {
  public let photo: [Photo]
  
  public init(photo: [Photo]) {
    self.photo = photo
  }
}

public struct Photo: Codable {
  public let id: String
  public let secret: String
  
  public init(id: String, secret: String) {
    self.id = id
    self.secret = secret
  }
}

public class DataFetcher {
  
  private var retrievedImages = [Image]()
  private var largeImage = "_b"
  private var largeSquareImage = "_q"
  private var directImageLink = "https://farm6.staticflickr.com/5800/"
  
  public init() {
    
  }
  
  public func getPopularItems(apiString: String, completionHandler: @escaping ([Image]) -> Void) {
    guard let itemUrl = URL(string: apiString) else {
      completionHandler(self.retrievedImages)
      return
    }
    let request = URLRequest(url: itemUrl)
    let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
      if let error = error {
        print(error)
        completionHandler(self.retrievedImages)
      }
      if let data = data {
        self.retrievedImages = self.parseJsonData(data: data)
        completionHandler(self.retrievedImages)
      }
    })
    task.resume()
  }
  
  func parseJsonData(data: Data) -> [Image] {
    var items = [Image]()
    do {
      let jsonResponseObject =  try JSONDecoder().decode(ResponseObject.self, from: data)
      for objects in jsonResponseObject.photos.photo {
        var newImage = Image()
        newImage.large = "\(directImageLink)\(objects.id)_\(objects.secret)\(largeImage).jpg"
        newImage.largeSquare = "\(directImageLink)\(objects.id)_\(objects.secret)\(largeSquareImage).jpg"
        items.append(newImage)
      }
    } catch {
      print(error)
    }
    
    return items
  }
}
