//
//  ViewController.swift
//  MinderaCodeTest
//
//  Created by Stuart Thomas on 14/01/2021.
//

import UIKit

class ViewController: UIViewController {
  
  let apiString: String = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="
  let apiKey: String = "f9cc014fa76b098f9e82f1c288379ea1"
  let endOfApiString: String = "&tags=kitten&page=1&format=json&nojsoncallback=1"
  var didShowError: Bool = false
  var isUsingCoreDataImages: Bool = false
  var images: [Image]?
  var coreDataImages: [ImageData]?
  
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var loadingLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    makeItemRequest(apiString: makeApiString())
  }
  
  func makeItemRequest(apiString: String) {
    let dataFetcher =  DataFetcher.init()
    dataFetcher.getPopularItems(apiString: apiString) { (images) in
      if !images.isEmpty {
        self.didShowError = false
        self.images = images
        DispatchQueue.main.async {
          self.loadingLabel.isHidden = true
          self.collectionView.reloadData()
        }
      } else {
        DispatchQueue.main.async {
          self.showError()
        }
      }
    }
  }
}

public struct Image {
  var largeSquare: String?
  var large: String?
}

public struct ImageData {
  var largeSquare: Data?
  var large: Data?
}
