//
//  ViewController+DataSource.swift
//  MinderaCodeTest
//
//  Created by Stuart Thomas on 18/01/2021.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView,
                      numberOfItemsInSection section: Int) -> Int {
    return images?.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                  for: indexPath as IndexPath) as! ImageCollectionViewCell
    if isUsingCoreDataImages {
      setupCellWithCoreDataImages(cell: cell, indexPathRow: indexPath.row)
    } else {
      setupCellWithAPIImages(cell: cell, indexPathRow: indexPath.row)
    }
    
    return cell
  }
  
  private func setupCellWithAPIImages(cell: ImageCollectionViewCell,
                                      indexPathRow: Int) {
    if let largeSquareImageUrlString = images?[indexPathRow].largeSquare,
       let largeImageUrlString = images?[indexPathRow].large,
       let largeSquareImageUrl = URL(string: largeSquareImageUrlString),
       let largeImageUrl = URL(string: largeImageUrlString) {
      let largeSquareImageData = try? Data(contentsOf: largeSquareImageUrl)
      let largeImageData = try? Data(contentsOf: largeImageUrl)
      saveImage(largeSquareImageData: largeSquareImageData!,
                largeImageData: largeImageData!)
      cell.imageView.image = UIImage(data: largeSquareImageData!)
    }
  }
  
  private func setupCellWithCoreDataImages(cell: ImageCollectionViewCell,
                                           indexPathRow: Int) {
    if indexPathRow < coreDataImages?.count ?? 0 {
      if let unwrappedImageData = coreDataImages?[indexPathRow].largeSquare {
        cell.imageView.image = UIImage(data: unwrappedImageData)
      }
    }
  }
  
  private func saveImage(largeSquareImageData: Data,
                         largeImageData: Data) {
    let coreDataHelper = CoreDataHelper()
    let imageData = ImageData(largeSquare: largeSquareImageData,
                              large: largeImageData)
    coreDataHelper.addNewImageData(imageData: imageData)
  }
}
