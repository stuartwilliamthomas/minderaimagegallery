//
//  CoreDataHelper.swift
//  MinderaCodeTest
//
//  Created by Stuart Thomas on 18/01/2021.
//

import Foundation
import CoreData
import UIKit

class CoreDataHelper {
  
  private var context: NSManagedObjectContext?
  private let entityName: String = "CoreDataImage"
  
  init() {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    context = appDelegate.persistentContainer.viewContext
  }
  
  func addNewImageData(imageData: ImageData) {
    if let context = context,
       let largeImage = imageData.large,
       let largeSquareImage = imageData.largeSquare {
      let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
      let newImageData = NSManagedObject(entity: entity!, insertInto: context)
      
      newImageData.setValue(largeImage, forKey: "large")
      newImageData.setValue(largeSquareImage, forKey: "largeSquare")
      
      save()
    }
  }
  
  func retrieve() -> [ImageData]? {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
    request.returnsObjectsAsFaults = false
    var imageDataArray = [ImageData]()
    do {
      let result = try context.fetch(request)
      for data in result as! [NSManagedObject] {
        let retrievedImageData = ImageData(largeSquare: data.value(forKey: "largeSquare") as? Data,
                                           large: data.value(forKey: "large") as? Data)
        imageDataArray.append(retrievedImageData)
      }
      return imageDataArray
    } catch {
      print("Retreive Failed")
      return nil
    }
  }
  
  private func save() {
    do {
      try context?.save()
    } catch let error as NSError  {
      print("Could not save \(error), \(error.userInfo)")
    }
  }
}

