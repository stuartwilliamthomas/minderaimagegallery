//
//  ViewController+Helper.swift
//  MinderaCodeTest
//
//  Created by Stuart Thomas on 18/01/2021.
//

import UIKit

extension ViewController {
  
  func makeApiString() -> String {
    return "\(apiString)\(apiKey)\(endOfApiString)"
  }
  
  func showError() {
    didShowError = true
    let alert = createAlert()
    self.present(alert, animated: true, completion: nil)
  }
  
  func handleTryAgain() {
    self.isUsingCoreDataImages = false
    self.makeItemRequest(apiString: self.makeApiString())
    self.loadingLabel.isHidden = false
  }
  
  func handleLoadCachedImages() {
    self.isUsingCoreDataImages = true
    let coreDataHelper = CoreDataHelper()
    self.coreDataImages = coreDataHelper.retrieve()
    self.collectionView.reloadData()
    self.loadingLabel.isHidden = true
  }
  
  private func createAlert() -> UIAlertController {
    let message = "Could not connect to API."
    let alert = UIAlertController(title: message,
                                  message: nil,
                                  preferredStyle: UIAlertController.Style.alert)
    addTryAgainAction(alert: alert)
    addLoadCachedImagesAction(alert: alert)
    return alert
  }
  
  private func addTryAgainAction(alert: UIAlertController) {
    alert.addAction(UIAlertAction(title: "Try Again",
                                  style: UIAlertAction.Style.default,
                                  handler: { (action: UIAlertAction) in
                                    self.handleTryAgain()
                                  }))
  }
  
  private func addLoadCachedImagesAction(alert: UIAlertController) {
    alert.addAction(UIAlertAction(title: "Load Cached Images",
                                  style: UIAlertAction.Style.default,
                                  handler: { (action: UIAlertAction) in
                                    self.handleLoadCachedImages()
                                  }))
  }
  
}
