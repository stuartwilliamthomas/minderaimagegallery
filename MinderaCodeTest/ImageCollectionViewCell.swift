//
//  ImageCollectionViewCell.swift
//  MinderaCodeTest
//
//  Created by Stuart Thomas on 14/01/2021.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var imageView: UIImageView!
  
}
